var express = require('express');
var nunjucks = require('nunjucks');
var path = require('path');
var conf = require('./config')

var app = express();

app.set('views', __dirname + '/templates')
app.set('view engine', 'nunjucks')


nunjucks.configure(__dirname+'/templates/', {
    autoescape: true,
    express: app
});

app.engine('html', nunjucks.render ) ;

app.use('/js', express.static(__dirname + '/static/js'));
app.use('/css', express.static(__dirname + '/static/styles'));
app.use('/assets', express.static(__dirname + '/static/assets'));
app.use('/lib', express.static(__dirname + '/lib/public'));
app.use('/bow', express.static(__dirname + '/bower_components'));
app.use('/bower_components', express.static(__dirname + '/bower_components'));


routes = require('./handler/routes')
app.use('/', routes)


app.listen(conf.get('PORT'), () => {
    console.log(`listening at PORT ${conf.get('PORT')}`)
});
