requirejs.config({
    baseUrl: '../../../bower_components/',
    paths: { // path to your app
        app: '../static/js/',
        zepto: 'zepto/zepto'
    },
    shim: {
       zepto: {
         exports: '$'
       },
    }
});
